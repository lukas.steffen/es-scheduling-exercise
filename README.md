# ES Scheduling Exercise

Hier findet sich das im Rahmen der Übung 9 im Fach Eingebettete Systeme verwendete Jupyter Notebook.
Der Hauptteil besteht darin, die besprochenen Algorithmen (FCFS, SJF, SRTN und Prioritätsscheduling) in Python zu implementieren.

Mit dem Notebook lässt sich auf mehrere Arten arbeiten:
- Lokal, mit `git clone`
- Im Browser, mittels Binder

Für eine Verwendung im Browser können folgende Links verwendet werden:
- Aufgaben: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.amd.e-technik.uni-rostock.de%2Flukas.steffen%2Fes-scheduling-exercise.git/HEAD?labpath=exercise9.ipynb

- Lösungen: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.amd.e-technik.uni-rostock.de%2Flukas.steffen%2Fes-scheduling-exercise.git/solution?labpath=exercise9.ipynb

Ich kann empfehlen, die Musterlösungen (oder Ihre eigene Implementierung) zu verwenden, um sich Beispielaufgaben zu generieren. Das können sie tun, indem sie die Liste `task_list` anpassen.